from django.urls import path

from . import views

urlpatterns = [
  path("", views.index, name = "index"),
  path("test/", views.test, name = "test"),
  path("train/", views.train, name = "train"),
  path("work/", views.work, name = "work"),
  # path("test_result/", views.test_result, name = "test_result"),
]