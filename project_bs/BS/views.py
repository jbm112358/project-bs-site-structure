from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from .forms import TestForm, TrainForm, WorkForm
import pandas as pd
import os
# Create your views here.

def index(request):
  return render(request, "BS/index.html")


def train(request):
  # if this is a POST request we need to process the form data
  keywords = ["brandy", "whisky", "nonbrandy", "port", "wine", "non-wine", "port salut", "beer", "beery beer", "islay"]

  if request.method == 'POST':
      # create a form instance and populate it with data from the request:
      form = TrainForm(request.POST)

      # check whether it's valid:
      if form.is_valid():
          # process the data in form.cleaned_data as required
          # ...
          # redirect to a new URL:
          for x, y in zip(keywords, form.cleaned_data):
            print([x,form.cleaned_data[y]])

          
          return render(request, 'BS/train_result.html')

  # if a GET (or any other method) we'll create a blank form
  else:
      form = TrainForm()

  return render(request, 'BS/train.html', {'form': form, 'keywords': keywords})



# def test(request):

#   return render(request, "BS/test.html")

def test(request):
  # if this is a POST request we need to process the form data
  if request.method == 'POST':
      # create a form instance and populate it with data from the request:
      form = TestForm(request.POST)
      # check whether it's valid:
      if form.is_valid():
          # process the data in form.cleaned_data as required
          # ...
          # redirect to a new URL:
          keyword = form.cleaned_data["test_term"]
          result = "brand"


          return render(request, 'BS/test_result.html', {"keyword": keyword, "result": result})

  # if a GET (or any other method) we'll create a blank form
  else:
      form = TestForm()


  return render(request, 'BS/test.html', {'form': form})

# def test_result(request):
#   return render(request, '/BS/test_result.html', {"keyword": keyword, "result": result})

def work(request):
  # if this is a POST request we need to process the form data
  if request.method == 'POST':
      # create a form instance and populate it with data from the request:
      form = WorkForm(request.POST, request.FILES)
      # check whether it's valid:
      if form.is_valid():
          # process the data in form.cleaned_data as required
          # ...
          file_name = str(request.FILES["csv"])
          dir_path = os.path.dirname(os.path.realpath(__file__))
          file = pd.read_csv(request.FILES["csv"])
          file.to_csv(f"{dir_path}/processing_files/{file_name}")


          return render(request, 'BS/work_processing.html',)

  # if a GET (or any other method) we'll create a blank form
  else:
      form = WorkForm()


  return render(request, 'BS/work.html', {'form': form})

