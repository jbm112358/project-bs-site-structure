from django import forms

class TestForm(forms.Form):
  test_term = forms.CharField(label = "Enter keyword", max_length = 100)

class TrainForm(forms.Form):
  choices = (("B", "Brand"), ("N", "Non-Brand"))
  term1 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term2 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term3 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term4 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term5 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term6 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term7 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term8 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term9 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)
  term10 = forms.CharField(widget=forms.RadioSelect(
               choices=choices), help_text="test", max_length = 128)

class WorkForm(forms.Form):
  csv = forms.FileField()